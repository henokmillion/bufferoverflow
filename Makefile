shell: shellcode.c
	gcc -o shell shellcode.c
asm_shell: asm_shellcode.c
	gcc -z execstack -o asm_shell asm_shellcode.c
stack: stack.c
	gcc -o stack -z execstack -fno-stack-protector stack.c
exploit: exploit.c
	gcc -o exp -z execstack -fno-stack-protector exploit.c
setuid: setuid.c
	gcc -o setuid setuid.c